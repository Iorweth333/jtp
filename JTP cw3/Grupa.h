//
// Created by Karol on 11.04.2017.
//

#ifndef JTP_CW3_GRUPA_H
#define JTP_CW3_GRUPA_H


#include <ostream>
#include "Osoba.h"

class Grupa {

private:
    Osoba *tab;
    int rozmiar;
    int licznik;

public:
    Grupa(const int rozmiar = 20);

    Grupa (const Grupa &grupa2);

    friend ostream &operator<<(ostream &os, const Grupa &grupa);

    friend ostream &operator<<(ostream &os, const Grupa *grupa);

    Grupa &operator= (const Grupa grupa2);

    int getLicznik() const {return licznik;}

    void dodaj_osobe (const Osoba &nowaOs);

    Osoba &operator[] (const int indeks);

    virtual ~Grupa();       //Określenie metody jako wirtualnej sprawia, że będzie wywoływana w wersji odpowiadającej rzeczywistemu typowi
};


#endif //JTP_CW3_GRUPA_H
