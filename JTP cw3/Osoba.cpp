//
// Created by Karol on 11.04.2017.
//

#include "Osoba.h"

Osoba::Osoba(const string &imie, int rok_ur) : imie(imie), rok_ur(rok_ur) {
    this->imie = imie;
    this->rok_ur = rok_ur;
}

ostream &operator<<(ostream &os, const Osoba &osoba){
    os << "imie: " << osoba.imie << " rok_ur: " << osoba.rok_ur;
    return os;
}

Osoba::Osoba(const Osoba &osoba2) {

    this->imie = osoba2.imie;
    this->rok_ur = osoba2.rok_ur;
}

Osoba &Osoba::operator=(const Osoba &osoba2) {

    imie = osoba2.imie;
    rok_ur = osoba2.rok_ur;

}
