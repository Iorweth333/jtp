//
// Created by Karol on 11.04.2017.
//

#include <iostream>
#include "Grupa.h"

Grupa::Grupa(const int rozmiar) {
    this->tab = new Osoba[rozmiar];
    this->rozmiar = rozmiar;
    this->licznik = 0;

}

ostream &operator<<(ostream &os, const Grupa &grupa){
    os << "Grupa osob:  rozmiar: " << grupa.rozmiar << " licznik: " << grupa.licznik << endl;

    for (int i = 0; i < grupa.rozmiar; i++) os << grupa.tab[i] << endl;
    return os;
}


ostream &operator<<(ostream &os, const Grupa *grupa){
    os << "Grupa osob:  rozmiar: " << grupa->rozmiar << " licznik: " << grupa->licznik << endl;

    for (int i = 0; i < grupa->rozmiar; i++) os << grupa->tab[i] << endl;
    return os;
}

Grupa::Grupa(const Grupa &grupa2) {
    this->rozmiar = grupa2.rozmiar;
    this->licznik = grupa2.licznik;

    this->tab = new Osoba[grupa2.rozmiar];

    for (int i = 0; i < this->rozmiar; i++) this->tab[i] = grupa2.tab[i];

}

Grupa &Grupa::operator=(const Grupa grupa2) {

    Grupa *res = new Grupa(grupa2);

    return *res;
}


void Grupa::dodaj_osobe(const Osoba &nowaOs) {

    if (this->rozmiar == this->licznik) {
        string wyjatek = "grupa jest pelna!";
        throw wyjatek;
    } else {
        tab[licznik] = nowaOs;
        this->licznik++;
        //cout<<"test dodawania osoby: tab[licznik]: "<<tab[licznik]<<endl;
    }
}

Grupa::~Grupa() {
    delete[] tab;
    rozmiar = 0;
    licznik = 0;
}

Osoba &Grupa::operator[](const int indeks) {
    if (indeks >= 0 and indeks < rozmiar) return tab[indeks];
    else {
        string wyjatek = "Bledny indeks!";
        throw wyjatek;
    }
}


