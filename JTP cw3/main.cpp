#include <iostream>
#include <conio.h>
#include "Osoba.h"
#include "Grupa.h"

int main() {

    Osoba osoba1("Karol", 1996);

    Osoba osoba2("Michal", 1996);

    Osoba osoba3("Monika", 1999);

    Osoba osoba4("Adam", 2010);

    Osoba osoba5("Pawel", 1997);

    Osoba osoba6("Marcin", 1998);

    cout << osoba1 << endl;

    Osoba osoba7= osoba1;
    cout << "test przypisania osoby: osoba7 = osoba1"<<endl << osoba7 << endl;

    Osoba *ostab = new Osoba[5];

    ostab[1] = osoba1;
    cout << "test przypisanie w tablicy: ostab[1] = osoba1" << endl<< ostab[1] << endl;

    Grupa grupa1(5);

    cout << "grupa przed dodawaniem osob: " << grupa1 << endl;


    try {
        grupa1.dodaj_osobe(osoba1);
        grupa1.dodaj_osobe(osoba2);
        grupa1.dodaj_osobe(osoba3);
        grupa1.dodaj_osobe(osoba4);
        grupa1.dodaj_osobe(osoba5);
    }
    catch (string wyjatek) {
        cout << "Nastapil wyjatek: " << wyjatek << endl;
        exit(1);
    }
    catch (...) {
        cout << "Nastapil nieznany wyjatek przy dodawaniu osob do grupy1" << endl;
        exit(1);
    }

    cout << "grupa po dodaniu osob " << grupa1 << endl;

    cout << endl << "test operatora przypisania grupy (konstruktor kopiujący): grupa2 = grupa1" << endl;
    Grupa grupa2 = grupa1;

    cout << grupa2 << endl;

    cout<<"test operatora indeksacji: grupa2[3] = osoba2"<<endl;

    grupa2[3] = osoba2;

    cout<<grupa2<<endl;

    cout<<"test blednego indeksu: grupa2[50] = osoba3"<<endl;

    try {grupa2[50] = osoba3;}
    catch (string wyjatek)
    {
        cout<<" Wystapil blad: "<<wyjatek<<endl<<endl;
    }

    cout << "test wyjatku przepelnienia grupy: " << endl;

    Grupa *grupa3 = new Grupa();


    cout << "grupa3 tuz po zadeklarowaniu: " << grupa3 << endl;

    cout << "dodawanie 20 razy jednej osoby: " << endl;

    try {
        for (int i = 0; i < 20; i++) {
            grupa3->dodaj_osobe(osoba1);
        }
    }
    catch (string wyjatek) {
        cout << wyjatek << endl;
    }

    cout<<grupa3<<endl;

    cout << "dodanie 21-szej osoby:" << endl;
    try {
        grupa3->dodaj_osobe(osoba1);

    }
    catch (string wyjatek) {
        cout << wyjatek << endl;
    }

    cout<<"test destruktora: "<<endl;

    delete grupa3;

    cout<<"jesli destrukcja sie powiodla, program sie wykrzaczy. Wcisnij dowolny przycisk aby kontynuowac"<<endl;

    while (!kbhit());

    grupa3->dodaj_osobe(osoba1);

    cout<<"Jesli ten komunikt sie wyswietla, to znaczy ze destrukcja sie nie powiodla"<<endl;

    while (!kbhit());

    return 0;
}