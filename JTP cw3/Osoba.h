//
// Created by Karol on 11.04.2017.
//

#ifndef JTP_CW3_OSOBA_H
#define JTP_CW3_OSOBA_H

#include <string>
#include <ostream>

using namespace std;

class Osoba {
private:
    string imie;
    int rok_ur;

public:
    Osoba(const string &imie = "BRAK IMIENIA", int rok_ur = 0);

    friend ostream &operator<<(ostream &os, const Osoba &osoba);

    Osoba (const Osoba &osoba2);

    Osoba &operator= (const Osoba &osoba2);

};


#endif //JTP_CW3_OSOBA_H
