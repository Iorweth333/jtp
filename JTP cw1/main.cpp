#include <iostream>
#include <math.h>
#include <string>
#include <iomanip>

using namespace std;


class Punkt
{
private:
    double x;
    double y;
    double z;
    static int licznik;


public:
    Punkt ();
    Punkt (double x, double y, double z);

    double getZ () { return z;}

    void setZ (double z) {this->z = z;}

    double odleglosc (Punkt that);

    void wypisz ();

    void zapisz(ostream &os);
    void wczytaj(istream &os);

    void zeruj_licznik () { licznik = 0;}

    int getLicznik () { return licznik;}
};

int Punkt::licznik = 0;

Punkt::Punkt()
{
    x=0;
    y=0;
    z=0;
    licznik++;
}

Punkt::Punkt(double x, double y=0, double z=0)
{
    this->x = x;
    this->y = y;
    this->z = z;

    licznik++;
}


double Punkt::odleglosc(Punkt that)
{
    double a = this->x - that.x;
    double b = this->y - that.y;
    double c = this->z - that.z;

    double a2 = a * a;
    double b2 = b * b;
    double c2 = c * c;

    return sqrt(a2 + b2 + c2);
}


void Punkt::wypisz()
{
    cout <<this->x<<", "<<this->y<<", "<<this->z<<endl;
}

void Punkt::wczytaj(istream &os)
{
    os>>x>>y>>z;
}

void Punkt::zapisz(ostream &os)
{
    long int x = (this->x ) * 1000;
    long int y = (this->y ) * 1000;
    long int z = (this->z ) * 1000;

    if (x % 10 >= 5) x += 1;
    if (y % 10 >= 5) y += 1;
    if (z % 10 >= 5) z += 1;

    //double zaokragloneX = (x / 10) * 0.01;
    //double zaokragloneY = (y / 10) * 0.01;
    //double zaokragloneZ = (z / 10) * 0.01;

    double zaokragloneX = x / 1000.0;
    double zaokragloneY = y / 1000.0;
    double zaokragloneZ = z / 1000.0;


    os<<"[";
    os<<setprecision(3)<<fixed;
    os<<zaokragloneX<<"; "<<zaokragloneY<<"; "<<zaokragloneZ<<"]";
}
/*
int Punkt::getLicznik() {
    return licznik;
}
*/

int main()
{
    Punkt P1, P2 (2,3,5);

    P1.wypisz();
    P2.wypisz();

    double odleglosc = P1.odleglosc(P2);
    cout<<"odleglosc miedzy punktami: " <<odleglosc<<endl;


    P1.setZ(1);
    cout<<"zmieniam Z punktu pierwszego na: " << P1.getZ()<<endl;

    P1.zapisz(cout);


    cout<<"\nwczytaj nowy P1: "<<endl;

    P1.wczytaj(cin);

    P1.zapisz(cout);

    Punkt P3, P4, P5, P6 (3.3, 2.0/3.0, 0.1);
    cout<<"\ntworze cztery nowe punkty. teraz jest ich: "<<P3.getLicznik()<<endl;

    P4.zeruj_licznik();
    cout<<"\nzeruje licznik. teraz licznik jest rowny: "<<P5.getLicznik()<<endl;


    cout<<"P6 ma wspolrzedne: ";
    P6.zapisz(cout);



    return 0;
}
