cmake_minimum_required(VERSION 3.7)
project(JTP_cw1)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp)
add_executable(JTP_cw1 ${SOURCE_FILES})