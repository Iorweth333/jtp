#include <iostream>
#include "lib/Osoba.h"
#include "lib/Pracownik.h"
#include "lib/Kierownik.h"
#include "lib/Stazysta.h"

using namespace std;
using namespace ksiegowosc;
int main()
{
    Osoba *tabO [3] = {new Osoba ("Karol", "Bielen",9,1,1996), new Osoba ("Dawid", "Wojcik", 18, 6, 1996), new Osoba ("Piotr", "Wielki", 9,6,1672) };
    Pracownik *tabP [2] = {new Pracownik ("Monika", "Len", 18,5,1980, 3150), new Pracownik ("Boguslaw", "Gorczyca", 12, 2, 1971, 2500)};
    Kierownik *tabK [2] = {new Kierownik ("Stanislawa", "Janiec", 13,3,1967,8000,1000), new Kierownik ("Grzegorz", "Sudol", 30,3,1978, 7500, 2000)};
    Stazysta *tabS [2] = {new Stazysta ("Hermiona", "Granger", 19,9, 1979, 1500, 29, 9, 2017), new Stazysta ("Emma", "Watson", 15,4,1990, 1600, 1, 10, 2017)};

    cout<<tabO[0]<<endl;
    cout<<"opis: "<< tabO[0]->opis()<<endl;

    tabP[0]->zapisz(cout);
    cout<<tabP[0]->wyplata()<<endl;
    cout<<tabP[1]->opis()<<endl;

    tabK[1]->zapisz(cout);

    cout<<tabK[0]->opis()<<endl;
    cout<<"Wyplata kierownika: "<<tabK[0]->wyplata()<<endl;

    cout<<"wyplata stazysty: \n"<<tabS[0]->wyplata()<<endl;


    cout<<"Test operatora wczytywania: "<<endl;

    Osoba osoba ("brak imienia", "brak nazwiska", -1,-1,-1);
    cout<<"przed wczytaniem: "<<osoba<<endl;
    cout<<"Wpisz nowe dane: "<<endl;
    osoba.wczytaj(cin);
    cout<<"po wczytaniu: "<<osoba<<endl;

    return 0;
}