//
// Created by karolb on 09.05.17.
//

#include "Pracownik.h"

namespace ksiegowosc {


    Pracownik::Pracownik(string imie, string nazwisko, int dzien, int miesiac, int rok, double pensja) : Osoba(imie,
                                                                                                               nazwisko,
                                                                                                               dzien,
                                                                                                               miesiac,
                                                                                                               rok)
    {
        this->pensja = pensja;
    }

    ostream &operator<<(ostream &os, const Pracownik &pracownik)
    {
        os << "Pracownik " << pracownik.imie << " " << pracownik.nazwisko << " o pensji: " << pracownik.pensja << endl;
        return os;
    }

    bool Pracownik::zapisz(ostream &os)
    {
        os << (*this);       //maybe some cast is necessary, CHECK IT!
        return os.good();
    }

}