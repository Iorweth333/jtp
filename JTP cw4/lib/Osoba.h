//
// Created by karolb on 09.05.17.
//

#ifndef JTPCW04_OSOBA_H
#define JTPCW04_OSOBA_H

#include <iostream>
#include <string>
#include "Data.h"

using namespace std;

namespace ksiegowosc {


    class Osoba
    {
    protected:
        string imie, nazwisko;
        Data data_ur;

    public:

        Osoba(string imie, string nazwisko, int dzien, int miesiac, int rok);

        virtual ~Osoba()
        {};

        string opis();

        virtual double wyplata() {};    //almost pure virtual
                                        //pure virtual should look like this: virtual double wyplata() = 0
                                        //but then i wouldn't be able to create objects of this class

        virtual bool zapisz(ostream &os);

        virtual bool wczytaj(istream &is);

        friend ostream &operator<<(ostream &os, const Osoba &osoba);


    };
}

#endif //JTPCW04_OSOBA_H
