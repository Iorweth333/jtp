//
// Created by karolb on 09.05.17.
//

#include "Kierownik.h"
namespace ksiegowosc {
    Kierownik::Kierownik(string imie, string nazwisko, int dzien, int miesiac, int rok, double pensja, double dodatek)
            : Pracownik(imie, nazwisko, dzien, miesiac, rok, pensja)
    {
        this->dodatek = dodatek;
    }

    double Kierownik::wyplata()
    {
        return pensja + dodatek;
    }
}