//
// Created by karolb on 09.05.17.
//

#include "Stazysta.h"

namespace ksiegowosc {

    Stazysta::Stazysta(string imie, string nazwisko, int dzien, int miesiac, int rok, double stypendium, int koniec_d,
                       int koniec_m, int koniec_r)
            : Osoba(imie, nazwisko, dzien, miesiac, rok),
              koniec_stazu(koniec_d, koniec_m, koniec_r)
    {
        this->stypendium = stypendium;
    }

    void Stazysta::zmien_koniec_stazu(const Data data)
    {
        koniec_stazu = data;
    }

    double Stazysta::wyplata()
    {
        return stypendium;
    }
}