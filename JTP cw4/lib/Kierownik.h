//
// Created by karolb on 09.05.17.
//

#ifndef JTPCW04_KIEORNIK_H
#define JTPCW04_KIEORNIK_H


#include "Pracownik.h"

namespace ksiegowosc {

    class Kierownik : public Pracownik
    {
    protected:
        double dodatek;

    public:
        Kierownik(string imie, string nazwisko, int dzien, int miesiac, int rok, double pensja, double dodatek);

        double wyplata();
    };

}

#endif //JTPCW04_KIEORNIK_H
