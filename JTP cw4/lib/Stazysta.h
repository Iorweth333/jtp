//
// Created by karolb on 09.05.17.
//

#ifndef JTPCW04_STAZYSTA_H
#define JTPCW04_STAZYSTA_H


#include "Data.h"
#include "Osoba.h"

namespace ksiegowosc {


    class Stazysta : public Osoba
    {
    protected:
        double stypendium;
        Data koniec_stazu;

    public:
        Stazysta(string imie, string nazwisko, int dzien, int miesiac, int rok, double stypendium, int koniec_d,
                 int koniec_m, int koniec_r);

        void zmien_koniec_stazu(Data data);

        double wyplata ();

    };

}
#endif //JTPCW04_STAZYSTA_H
