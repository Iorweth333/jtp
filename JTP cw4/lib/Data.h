//
// Created by karolb on 09.05.17.
//

#ifndef JTPCW04_DATA_H
#define JTPCW04_DATA_H

#include <iostream>

namespace ksiegowosc {

    class Data
    {

    private:
        int dzien, miesiac, rok;

    public:

        Data(int dzien, int miesiac, int rok);

        void setDzien(int dzien)
        { this->dzien = dzien; }

        int getDzien()
        { return dzien; };

        friend std::ostream &operator<<(std::ostream &os, const Data &data);

        bool zapisz(std::ostream &os);

        bool wczytaj(std::istream &is);

        std::string to_string();

        std::string int_to_string(int a);

    };

}
#endif //JTPCW04_DATA_H
