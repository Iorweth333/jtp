//
// Created by karolb on 09.05.17.
//

#include "Osoba.h"


namespace ksiegowosc {


    Osoba::Osoba(string imie, string nazwisko, int d, int m, int r)
            :
            data_ur(d, m, r)
    {
        this->imie = imie;
        this->nazwisko = nazwisko;
        this->data_ur = data_ur;
    }

    string Osoba::opis()
    {
        return "Osoba: " + imie + " " + nazwisko + " " + data_ur.to_string() + "\n";
    }

    bool Osoba::zapisz(ostream &os)
    {
        os << (*this);  //maybe some cast is necessary, CHECK IT!
        return os.good();
    }

    bool Osoba::wczytaj(istream &is)
    {
        is >> imie >> nazwisko;
        data_ur.wczytaj(is);
        return is.good();
    }

    ostream &operator<<(ostream &os, const Osoba &osoba)
    {
        os << "imie: " << osoba.imie << " nazwisko: " << osoba.nazwisko << " data_ur: " << osoba.data_ur << endl;
        return os;
    }
}