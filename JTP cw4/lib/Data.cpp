//
// Created by karolb on 09.05.17.
//

#include "Data.h"
#include <sstream>
#include <iostream>

using namespace std;
namespace ksiegowosc {


    std::ostream &operator<<(std::ostream &os, const Data &data)
    {
        os << "dzien: " << data.dzien << " miesiac: " << data.miesiac << " rok: " << data.rok;
        return os;
    }

    Data::Data(int dzien, int miesiac, int rok)
    {
        this->dzien = dzien;
        this->miesiac = miesiac;
        this->rok = rok;
    }

    bool Data::wczytaj(istream &is)
    {
        is >> dzien >> miesiac >> rok;
        return is.good();
    }

    bool Data::zapisz(ostream &os)
    {
        os << "dzien: " << dzien << " miesiac: " << miesiac << " rok: " << rok;
        return os.good();
    }

    string Data::to_string()
    {
        return "Data: " + int_to_string(dzien) + "." + int_to_string(miesiac) + "." + int_to_string(rok);

        //string d = static_cast<ostringstream*>( &(ostringstream() << dzien) )->str()
    }

    string Data::int_to_string(int a)
    {
        string Result;          // string which will contain the result

        ostringstream convert;   // stream used for the conversion

        convert << a;      // insert the textual representation of 'Number' in the characters in the stream

        Result = convert.str(); // set 'Result' to the contents of the stream
        return Result;
    }
}