//
// Created by karolb on 09.05.17.
//

#ifndef JTPCW04_PRACOWNIK_H
#define JTPCW04_PRACOWNIK_H


#include <ostream>
#include "Osoba.h"

namespace  ksiegowosc {


    class Pracownik : public Osoba
    {
    protected:
        double pensja;

    public:
        Pracownik(string imie, string nazwisko, int dzien, int miesiac, int rok, double pensja);

        double wyplata()
        { return pensja; }

        bool zapisz(ostream &os);

        friend ostream &operator<<(ostream &os, const Pracownik &pracownik);
    };

}
#endif //JTPCW04_PRACOWNIK_H
