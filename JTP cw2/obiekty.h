#ifndef OBIEKTY_H_INCLUDED
#define OBIEKTY_H_INCLUDED

#include <iostream>

using namespace std;

class Macierz;

class Punkt
{
private:
public:
    double getX() const;

    void setX(double x);

    double getY() const;

    void setY(double y);

private:
    double x, y;

public:
    Punkt (double x=0, double y=0)
    {
        this->x = x;
        this->y = y;
    }

    Punkt operator- (const Punkt &p);

    friend Macierz operator* (const Punkt &p);

    friend ostream& operator<< (ostream &wyjscie, const Punkt &p);

};
//-------------------------------------------------------------------------------------

class Macierz
{
private:
    double mxx, myy, mxy;

public:

    Macierz (double mxx=0, double mxy=0, double myy=0);



    Macierz operator- (const Macierz &prawa);

    void operator-= (const Macierz &prawa);

    Macierz operator* (const Punkt &p);

    void operator-- ();

    void operator-- (int x);   //postfix

    friend ostream& operator<< (ostream &wyjscie, const Macierz &m);

    friend Macierz operator* (const double &d, const Macierz &m);
    friend Macierz operator* (const Macierz &m, const double &d);
};


ostream& operator<< (ostream &wyjscie, const Macierz &m);

Macierz operator* (const double &d, const Macierz &m);

Macierz operator* (const Macierz &m, const double &d);




//METODY PUNKTu:

ostream &operator<< (ostream &wyjscie, const Punkt &p);





#endif // OBIEKTY_H_INCLUDED
