#include "obiekty.h"
#include <iostream>

using namespace std;

ostream &operator<<(ostream &wyjscie, const Punkt &p) {
    wyjscie << "Wpolrzedne: x = " << p.x << "   y = " << p.y << endl;
    return wyjscie;
}

ostream &operator<<(ostream &wyjscie, const Macierz &m) {
    wyjscie << "Macierz: " << endl << m.mxx << " " << m.mxy << endl << m.mxy << " " << m.myy << endl;
    return wyjscie;
}

void Macierz::operator-=(const Macierz &prawa) {
    this->mxx -= prawa.mxx;
    this->mxy -= prawa.mxy;
    this->myy -= prawa.myy;
}


Macierz::Macierz(double mxx, double mxy, double myy) {
    this->mxx = mxx;
    this->mxy = mxy;
    this->myy = myy;
}

Macierz Macierz::operator-(const Macierz &prawa) {
    Macierz wynik(this->mxx - prawa.mxx, this->mxy - prawa.mxy, this->myy - prawa.myy);
    return wynik;
}

Macierz Macierz::operator*(const Punkt &p) {
    Macierz wynik;

    wynik.myy = 0;      //wiem, ze deafultowo takie jest, ale jest czytelniej
    wynik.mxx = p.getX() * mxx + p.getY() * mxy;
    wynik.mxy = p.getX() * mxy + p.getY() * myy;

    return wynik;
}


Macierz operator*(const double &d, const Macierz &m) {
    Macierz wynik (m.mxx * d, m.myy * d, m.mxy * d);
    return wynik;
}

Macierz operator*(const Macierz &m, const double &d) {
    Macierz wynik (m.mxx * d, m.myy * d, m.mxy * d);
    return wynik;
}


void Macierz::operator--()        //prefix
{
    this->mxx--;
    this->mxy--;
    this->myy--;
}


void Macierz::operator--(int x)   //postfix
{
    this->mxx--;
    this->mxy--;
    this->myy--;
}


Punkt Punkt::operator-(const Punkt &p) {
    return Punkt(-1 * p.x, -1 * p.y);
}

double Punkt::getX() const {
    return x;
}

void Punkt::setX(double x) {
    Punkt::x = x;
}

double Punkt::getY() const {
    return y;
}

void Punkt::setY(double y) {
    Punkt::y = y;
}

